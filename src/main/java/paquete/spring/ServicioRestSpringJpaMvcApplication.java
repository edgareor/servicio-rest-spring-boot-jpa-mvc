package paquete.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioRestSpringJpaMvcApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ServicioRestSpringJpaMvcApplication.class, args);
	}
}